# Level Meter Tour Planning

This project provides an insight into condition-based route planning for the collection of recycling containers and document bins. It works in conjunction with an IoT sensor that continuously monitors the fill level of the bins via a time-of-flight sensor and transmits this to a cloud system. 

The project consists of the following functionality:

- read from Culumocity API for reading out IOT sensor information about filling levels and geo positions.
- usage of nomination server for getting a route description from the sensor with the highest filling level to the one with the lowest
- visualize the information via folium on a flask based website ([screenshots](./docs/screenshots.md))

This project is written in Python 3.11. Feel free to participate and enhance the available functionality.


## Disclaimer

This project does not come with any warranty. Use on own risk.

## License

Copyright Open Logistics Foundation

Licensed under the Open Logistics Foundation License 1.3
For details on the licensing terms, see the LICENSE file.
SPDX-License-Identifier: OLFL-1.3

See the LICENSE file in top directory.

## Licenses of third-party dependencies
The licenses used by this project's third-party dependencies are documented in the src/third-party-licenses directory.
This is done to encourage developers to check the licenses used by the third-party dependencies to ensure they do not conflict with the license of this project itself.
The directory contains the following files:

third-party-licenses.csv - Contains the licenses used by this project's third-party dependencies.
The content of this file is/can be generated with pip-licenses:

	pip-licenses --ignore-packages pkg-resources --with-urls --format=csv --output-file=third-party-licenses/third-party-licenses.csv

The content of this file is maintained manually.

## Overview

This readme shall give an overview about all topics being available. Please jump to the points of interest by using the links.

### Nominatim API usage / server setup for local usage

For an overview about Nominatim usage, implementation and setup follow this [link](./docs/nominatim.md).

### Environment

For information about the environment settings, what kind of infrastructure is necessary and how to make it run, have a look [here](./docs/environment.md).

### Start & Debugging

If you feel the need for debugging, have a look [here](./docs/start-and-debugging.md) for more information.

### Limitations

For known limitations and further links have a look onto this [link](./docs/limitations-and-links.md).

