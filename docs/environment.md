# Environment 

For implementation of this program, [Visual Studio Code](https://code.visualstudio.com/) has been used. Visual Studio Code is free of charge and has a large community, comes with tones of easy-to-install plugins and is easy to use.

## Environment variables

Have a look onto python file [env_secrets_template.py](../src/env_secrets_template.py). This file describes which environment information is necessary. It is not used within the program, it just gives and overview about the necessary information of the program.

If you use Visual Studio code, add and .env file below the .vscode folder with that very information:

```python
CUMULOCITY_USER_NAME=peterparker@spiderman.com
CUMULOCITY_USER_PASSWORD=very-secret-password
CUMULOCITY_BASE_URL=https://peterparker.ram.m2m.telekom.com
NOMINATIM_OPEN_STREET_MAP_BASE_URL=http://localhost:8080
```

## set up python program

Before starting, choose your Python Interpreter in Visual Studio Code by pressing ctrl+shift+P:Python: Select Interpreter.
Afterwards open a terminal and enter
```bash
cd ./src
pip install -r requirements.txt
```

After successful installation and setting up the environment variables the program is ready to being started.

```Python
python3 main.py
```

## Cumulocity

The cumulocity api is used for getting information from IOT sensors. This api is being provided by [Software AG](https://cumulocity.com/api/). This api is as well used by Telekom Cloud that is used in the environment variable base url above.

## Debugging

For information about debugging have a look at [Debugging](start-and-debugging.md).
