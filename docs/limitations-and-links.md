# limitations, known issues and links

Find limitations, known issues and links in the chapter.

## known issues/ features

There are two features that still need to be implemented:

- the route description cannot be downloaded by now
- when clicking an item of a visualized sensor, the position is just been shown as coordinates. This shall be replaced by calls against nominatim api to show the actual street/ city.

## limitations

Selecting dates is limited to 30 days. This is a limitation of the cumulocity api.

## links

[Cumulocity api guide and Postman collection](https://cumulocity.com/guides/microservice-sdk/rest/)


