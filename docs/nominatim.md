# Nominatim setup and usage

For determining the necessary route descriptions, Nominatim had been chosen. Nominiatim is free of charge, can be easily set up locally or in network. It is fast and reliable and easy to use.

## Overview

Have a look onto the official page for an overview of [Nominatim](https://nominatim.openstreetmap.org/ui/search.html).

## Installation

Setting up Nominatim is straight forward locally when using Docker. There is an open-sourced github repository available that maintains Docker Images for easy setup. Have a look onto this [link](https://github.com/mediagis/nominatim-docker) to get information how to retrieve the necessary open street map files to download and how to set up nominatim for usage.

Keep in mind that is it necessary to set up nominatim for route descriptions.

## Usage

The program needs to know the url for accessing Nominatim and retrieving information about the route description. The only information necessary is the base url. This needs to be provided as an environment variable. Have a look onto this [link](start-and-debugging.md) how to do this in visual studio code.

