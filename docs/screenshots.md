# Screenshots

To see the program in actions have a look onto the following screenshots.

## Start of the application

When starting the application it welcomes you with like the following:
![Start](./screenshots/start.png)

## Getting data

Just use the date picker to select the dates that shall be used to retrieve data from the cumulocity api of your choice:
![Selecting dates](./screenshots/select-dates.png)

Press the load button to retrieve data.
It will take up to 30 seconds to retrieve information and create an html page with the map in question.

## Results

When data has been read, the sensors will be visualized with a route description.
![route description](./screenshots/visualization-with-route-and-selected-item.png)

