# Debugging in Visual Studio Code

For installing [Visual Studio Code](https://code.visualstudio.com/) visit this website. 
Debugging is already prepared and easy to execute.

## Environment

The repository already comes with the .vscode folder available. In that folder the [launch.json](../.vscode/launch.json) file is available and ready to use.
The information that you need to provide is:

- define the .env file in .vscode folder to provider the necessary run time information

```json
{
  "version": "0.2.0",
  "configurations": [
    {
      "name": "Python: Current File",
      "type": "python",
      "request": "launch",
      "program": "main.py",
      "console": "integratedTerminal",
      "justMyCode": true,
      "args": [
      ],
      "cwd": "${workspaceFolder}/src",
      "envFile": "${workspaceFolder}/.vscode/.env"
    }
  ]
}
```

After defining the environment variables in .env file, just hit f5. Visual Studio Code starts the main.py.

## Start the program

Either you start the program with the debugging features of Visual Studio Code or via terminal:

```bash
cd src
python main.py  
```

You should see something like:

```bash
 * Serving Flask app 'main'
 * Debug mode: on
WARNING: This is a development server. Do not use it in a production deployment. Use a production WSGI server instead.
 * Running on http://127.0.0.1:8888
Press CTRL+C to quit
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: 139-657-791
 ```

 You are ready to use the browser of your choice to use the application under the specified url: ```http://127.0.0.1:8888```.