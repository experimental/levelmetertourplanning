# -*- coding: utf-8 -*-
"""
Copyright Open Logistics Foundation

Licensed under the Open Logistics Foundation License 1.3
For details on the licensing terms, see the LICENSE file.
SPDX-License-Identifier: OLFL-1.3

@author: holger leichsenring
"""

"""Module for defining recurring string that shall be maintained in one central place"""


class Constants:
    ENVIRONMENT_VARIABLE_CUMULOCITY_USER_NAME = "CUMULOCITY_USER_NAME"
    ENVIRONMENT_VARIABLE_CUMULOCITY_USER_PASSWORD = "CUMULOCITY_USER_PASSWORD"
    ENVIRONMENT_VARIABLE_CUMULOCITY_BASE_URL = "CUMULOCITY_BASE_URL"
    ENVIRONMENT_VARIABLE_NOMINATIM_OPEN_STREET_MAP_BASE_URL = "NOMINATIM_OPEN_STREET_MAP_BASE_URL"

    def __init__(self):
        pass
