# -*- coding: utf-8 -*-
"""
Copyright Open Logistics Foundation

Licensed under the Open Logistics Foundation License 1.3
For details on the licensing terms, see the LICENSE file.
SPDX-License-Identifier: OLFL-1.3

@author: holger leichsenring
"""

"""Reads and provides environment variables"""
import logging
import os

from core.constants import Constants


class EnvironmentVariableHandler:
    """Reads and provides environment variables"""

    # pylint: disable=fixme
    def __init__(self):
        self.constants = Constants()

    def validate(self):
        cumulocity_password = self.cumulocity_user_password
        cumulocity_user_name = self.cumulocity_user_name
        cumulocity_base_url = self.cumulocity_base_url
        nominatim_open_street_map_base_url = self.nominatim_open_street_map_base_url

        if cumulocity_user_name is None or cumulocity_password is None or cumulocity_base_url is None or nominatim_open_street_map_base_url is None:
            raise ValueError(
                f"Please specify {self.constants.ENVIRONMENT_VARIABLE_CUMULOCITY_USER_PASSWORD}, "
                + f"{self.constants.ENVIRONMENT_VARIABLE_CUMULOCITY_USER_NAME} and "
                + f"{self.constants.ENVIRONMENT_VARIABLE_CUMULOCITY_BASE_URL} and "
                + f"{self.constants.ENVIRONMENT_VARIABLE_NOMINATIM_OPEN_STREET_MAP_BASE_URL} as environment variables."
            )

        logging.info(
            "Environment variables for cumulocity connection are specified."
        )

    @property
    def cumulocity_user_name(self):
        return os.environ.get(self.constants.ENVIRONMENT_VARIABLE_CUMULOCITY_USER_NAME)

    @property
    def cumulocity_user_password(self):
        return os.environ.get(self.constants.ENVIRONMENT_VARIABLE_CUMULOCITY_USER_PASSWORD)

    @property
    def cumulocity_base_url(self):
        base_url = os.environ.get(self.constants.ENVIRONMENT_VARIABLE_CUMULOCITY_BASE_URL)
        if base_url is None:
            return None
        return base_url.strip('/')

    @property
    def nominatim_open_street_map_base_url(self):
        base_url = os.environ.get(self.constants.ENVIRONMENT_VARIABLE_NOMINATIM_OPEN_STREET_MAP_BASE_URL)
        if base_url is None:
            return None
        return base_url.strip('/')
