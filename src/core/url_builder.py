# -*- coding: utf-8 -*-
"""
Copyright Open Logistics Foundation

Licensed under the Open Logistics Foundation License 1.3
For details on the licensing terms, see the LICENSE file.
SPDX-License-Identifier: OLFL-1.3

@author: holger leichsenring
"""

"""Creates urls by parameter"""
from core.environment_variable_handler import EnvironmentVariableHandler


class UrlBuilder:
    """Creates urls by parameter"""

    def __init__(self):
        self.environment_variable_handler = EnvironmentVariableHandler()
        self.environment_variable_handler.validate()

    def create_get_devices(self, page_size):
        return f"{self.environment_variable_handler.cumulocity_base_url}/inventory/managedObjects?fragmentType=c8y_IsDevice&pageSize={page_size}"

    def create_get_groups(self, page_size):
        return f"{self.environment_variable_handler.cumulocity_base_url}/inventory/managedObjects?fragmentType=c8y_IsDeviceGroup&pageSize={page_size}"

    def create_get_measurements(self, date_from, date_to, page_size):
        return f"{self.environment_variable_handler.cumulocity_base_url}/measurement/measurements?dateFrom={date_from}&dateTo={date_to}&pageSize={page_size}"

    def create_get_distance_matrix(self):
        return f"{self.environment_variable_handler.nominatim_open_street_map_base_url}/ors/v2/matrix/driving-hgv"

    def create_get_distance_geo(self):
        return f"{self.environment_variable_handler.nominatim_open_street_map_base_url}/ors/v2/directions/driving-hgv/geojson"
