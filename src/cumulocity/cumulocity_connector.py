# -*- coding: utf-8 -*-
"""
Copyright Open Logistics Foundation

Licensed under the Open Logistics Foundation License 1.3
For details on the licensing terms, see the LICENSE file.
SPDX-License-Identifier: OLFL-1.3

@author: holger leichsenring
"""
import requests
import urllib3
from core.environment_variable_handler import EnvironmentVariableHandler
from core.url_builder import UrlBuilder
from models.cumulocity.device_collection import DevicePage
from models.cumulocity.group_collection import GroupPage
from models.cumulocity.measurement_collection import MeasurementPage

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


class CumulocityConnector():
    '''
    Connect to the Cumulocity API, in sample case the TelekomCloud, read out data
    '''

    def __init__(self):
        self.url_builder = UrlBuilder()
        self.environment_variable_handler = EnvironmentVariableHandler()
        self.environment_variable_handler.validate()

        self.cumulocity_base_url = self.environment_variable_handler.cumulocity_base_url
        self.cumulocity_user_name = self.environment_variable_handler.cumulocity_user_name
        self.cumulocity_password = self.environment_variable_handler.cumulocity_user_password

    def get_managed_object_devices(self):
        page_size = 1000
        url = self.url_builder.create_get_devices(page_size)
        auth = (self.cumulocity_user_name, self.cumulocity_password)
        managed_objects = []

        while True:
            response = requests.request(method="GET",
                                        url=url,
                                        data={},
                                        auth=auth,
                                        verify=False)
            if not response.ok:
                response.raise_for_status()

            json = response.json()
            page = DevicePage.from_dict(json)
            managed_objects.extend(page.managed_objects)

            if page.next:
                url = page.next
            else:
                break

        return managed_objects

    def get_managed_object_groups(self):
        page_size = 1000
        url = self.url_builder.create_get_groups(page_size)
        auth = (self.cumulocity_user_name, self.cumulocity_password)
        managed_objects = []

        while True:
            response = requests.request(method="GET",
                                        url=url,
                                        data={},
                                        auth=auth,
                                        verify=False)
            if not response.ok:
                response.raise_for_status()

            json = response.json()
            page = GroupPage.from_dict(json)
            managed_objects.extend(page.managed_objects)

            if page.next:
                url = page.next
            else:
                break

        return managed_objects

    def get_measurements(self, date_from, date_to):
        page_size = 1000
        url = self.url_builder.create_get_measurements(date_from, date_to, page_size)
        auth = (self.cumulocity_user_name, self.cumulocity_password)

        measurements = []

        while True:
            response = requests.request(method="GET",
                                        url=url,
                                        data={},
                                        auth=auth,
                                        verify=False)
            if not response.ok:
                response.raise_for_status()

            json = response.json()
            page = MeasurementPage.from_dict(json)
            measurements.extend(page.measurements)

            if page.next:
                url = page.next
            else:
                break

        return measurements
