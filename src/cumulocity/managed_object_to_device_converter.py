# -*- coding: utf-8 -*-
"""
Copyright Open Logistics Foundation

Licensed under the Open Logistics Foundation License 1.3
For details on the licensing terms, see the LICENSE file.
SPDX-License-Identifier: OLFL-1.3

@author: holger leichsenring
"""
from models.device import Device


class ManagedObjectToDeviceConverter():
    '''
    Convert Cumulocity managed objects in device objects
    '''

    def __init__(self):
        pass

    def convert(self, managed_objects):
        '''
        Convert Cumulocity managed objects in device objects
        '''
        devices = []
        for managed_object in managed_objects:
            device = Device()
            # had been different in original code, toil_configuration instead of toil_remote_configuration
            if managed_object.toil_remote_configuration:
                toil_remote_configuration = managed_object.toil_remote_configuration
                if toil_remote_configuration.configurations:
                    # get newest configuration
                    configuration = toil_remote_configuration.configurations[-1]
                    configuration_parameters = configuration.parameters
                    device.hysterese = configuration_parameters.distance_hysteresis_threshold
                    device.critical = configuration_parameters.distance_critical_threshold
                    device.warning = configuration_parameters.distance_warning_threshold
                    device.empty = configuration_parameters.distance_empty_threshold

            if managed_object.c8_y_position:
                c8_y_position = managed_object.c8_y_position
                device.lon = c8_y_position.lng
                device.lat = c8_y_position.lat
                device.time = c8_y_position.time

            if managed_object.name:
                # todo: specify a name when no name is available
                device.name = managed_object.name
            if managed_object.id:
                # todo: specify an id when no id is available
                device.id = managed_object.id

            if managed_object.child_assets:
                child_assets = managed_object.child_assets
                if child_assets.references:
                    references = child_assets.references
                    child_managed_object_reference = references[-1]
                    if child_managed_object_reference.managed_object:
                        child_managed_object = child_managed_object_reference.managed_object
                        if child_managed_object.name:
                            # todo: specify a name when no name is available
                            device.child_name = child_managed_object.name
                        if child_managed_object.id:
                            # todo: specify an id when no id is available
                            device.child_id = child_managed_object.id
            devices.append(device)
        return devices
