# -*- coding: utf-8 -*-
"""
Copyright Open Logistics Foundation

Licensed under the Open Logistics Foundation License 1.3
For details on the licensing terms, see the LICENSE file.
SPDX-License-Identifier: OLFL-1.3

@author: holger leichsenring
"""
from models.group import Group


class ManagedObjectToGroupConverter():
    '''
    Convert Cumulocity managed objects in group objects
    '''

    def __init__(self):
        pass

    def convert(self, managed_objects):
        '''
        Convert Cumulocity managed objects in group objects
        '''
        groups = []
        for managed_object in managed_objects:
            group = Group()

            if managed_object.name:
                # todo: specify a name when no name is available
                group.name = managed_object.name
            if managed_object.id:
                # todo: specify an id when no id is available
                group.id = managed_object.id

            if managed_object.child_assets:
                child_assets = managed_object.child_assets
                if child_assets.references:
                    references = child_assets.references
                    child_managed_object_reference = references[-1]
                    if child_managed_object_reference.managed_object:
                        child_managed_object = child_managed_object_reference.managed_object
                        if child_managed_object.name:
                            # todo: specify a name when no name is available
                            group.child_name = child_managed_object.name
                        if child_managed_object.id:
                            # todo: specify an id when no id is available
                            group.child_id = child_managed_object.id
            groups.append(group)
        return groups
