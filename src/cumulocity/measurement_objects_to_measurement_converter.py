# -*- coding: utf-8 -*-
"""
Copyright Open Logistics Foundation

Licensed under the Open Logistics Foundation License 1.3
For details on the licensing terms, see the LICENSE file.
SPDX-License-Identifier: OLFL-1.3

@author: holger leichsenring
"""
from models.measurement import Measurement


class MeasurementObjectsToMeasurementConverter():
    '''
    Convert Cumulocity measurement objects in measurement objects
    '''

    def __init__(self):
        pass

    def convert(self, measurement_objects):
        '''
        Convert Cumulocity measurement objects in measurement objects
        '''
        measurements = []

        for measurement_object in measurement_objects:
            if measurement_object.toil_level_meter_measurement:
                # only add measurements with data
                measurement = Measurement()
                measurement.id = measurement_object.id
                toil_level_meter_measurement = measurement_object.toil_level_meter_measurement

                if toil_level_meter_measurement.current_distance:
                    current_distance = toil_level_meter_measurement.current_distance
                    measurement.unit = current_distance.unit
                    measurement.value = current_distance.value

                measurement.time = measurement_object.time
                if measurement_object.source:
                    source = measurement_object.source
                    measurement.source_id = source.id
                measurements.append(measurement)
        return measurements
