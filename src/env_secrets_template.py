# -*- coding: utf-8 -*-
"""
Copyright Open Logistics Foundation

Licensed under the Open Logistics Foundation License 1.3
For details on the licensing terms, see the LICENSE file.
SPDX-License-Identifier: OLFL-1.3

@author: holger leichsenring
"""

"""Module for defining secrets"""
ENVIRONMENT_VARIABLE_CUMULOCITY_USER_NAME = "{email of the user authorized to access cumulocity provider}"
ENVIRONMENT_VARIABLE_CUMULOCITY_PASSWORD = "{password of this user}"
ENVIRONMENT_VARIABLE_CUMULOCITY_BASE_URL = "{base url to cumulocity provider}"
ENVIRONMENT_VARIABLE_NOMINATIM_OPEN_STREET_MAP_BASE_URL = "[base url to open street nomination server}"