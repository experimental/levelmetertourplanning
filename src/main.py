# -*- coding: utf-8 -*-
"""
Copyright Open Logistics Foundation

Licensed under the Open Logistics Foundation License 1.3
For details on the licensing terms, see the LICENSE file.
SPDX-License-Identifier: OLFL-1.3

@author: holger leichsenring
"""

import json
import os
from flask import Flask, render_template, send_from_directory, request

from map_creation.map_creator import MapCreator

app = Flask(__name__)
app.secret_key = ''


@app.route('/')
def index():
    return render_template('index.html', title='Level Meter Tour Planning')


@app.route('/get_map')
def get_map():
    base_dir = './'
    path = os.path.join(base_dir, 'templates/maps/map.html')
    if os.path.exists(path):
        return send_from_directory(os.path.join(base_dir, 'templates/maps/'), 'map.html')
    return send_from_directory(os.path.join(base_dir, 'templates/maps/'), 'empty_map.html')


@app.route('/create_map')
def create_map():
    date_from = request.args.get('dateFrom', None)
    date_to = request.args.get('dateTo', None)
    MapCreator().create(date_from, date_to)
    return json.dumps({'success': True}), 200, {'ContentType': 'application/json'}


@app.route('/download')
def download():
    base_dir = './'
    last_file = f'Wegpunkte.xlsx'
    return send_from_directory(os.path.join(base_dir, 'downloads'), last_file, as_attachment=True)


if __name__ == '__main__':
    app.run(debug=True, port=8888)
