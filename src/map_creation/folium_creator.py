# -*- coding: utf-8 -*-
"""
Copyright Open Logistics Foundation

Licensed under the Open Logistics Foundation License 1.3
For details on the licensing terms, see the LICENSE file.
SPDX-License-Identifier: OLFL-1.3

@author: holger leichsenring
"""
import folium
import folium.plugins
import os


class FoliumCreator():
    '''
    Creates html via folium
    '''

    def __init__(self):
        self.map = None
        self.feature_group_green = None
        self.feature_group_orange = None
        self.feature_group_red = None
        self.color_green = 'green'
        self.color_red = 'red'
        self.color_orange = 'orange'
        self.route = None

    def create_map(self):
        self.map = folium.Map(location=[51.2878, 9.4706], tiles="OpenStreetMap", zoom_start=6, detect_retina=True)
        return self

    def define_tile_layer(self):
        folium.TileLayer(tiles='Stamen Toner', name='Legende' + ' .' * 40).add_to(self.map)
        return self

    def create_feature_groups(self):
        self.feature_group_green = folium.FeatureGroup(name='Unkritisch')
        self.feature_group_orange = folium.FeatureGroup(name='Warnschwelle überschritten')
        self.feature_group_red = folium.FeatureGroup(name='Kritische Schwelle überschritten')
        return self

    def create_route(self):
        self.route = folium.FeatureGroup(name='Beispielroute entlang der kritischsten Container')
        return self

    def add_marker(self, lat, lon, popup_text, add_to, color):
        folium.Marker(location=[lat, lon],
                      popup=folium.Popup(popup_text, max_width=500),
                      icon=folium.Icon(color=color, icon='glyphicon glyphicon-trash')
                      ).add_to(add_to)
        return self

    def geo_json(self, geo_json, add_to):
        folium.GeoJson(geo_json).add_to(add_to)
        return self

    def add_markers_to_map(self):
        self.route.add_to(self.map)
        self.feature_group_green.add_to(self.map)
        self.feature_group_orange.add_to(self.map)
        self.feature_group_red.add_to(self.map)
        return self

    def configure_plugins(self):
        folium.plugins.Fullscreen().add_to(self.map)
        folium.plugins.MousePosition().add_to(self.map)
        return self

    def configure_layer_control(self):
        folium.LayerControl(collapsed=False).add_to(self.map)
        return self

    def fit_bounds(self, min_coordinates, max_coordinates):
        self.map.fit_bounds([min_coordinates, max_coordinates])
        return self

    def save_map(self, path):
        if os.path.exists(path):
            os.unlink(path)
        self.map.save(path)
        return self
