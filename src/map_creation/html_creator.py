# -*- coding: utf-8 -*-
"""
Copyright Open Logistics Foundation

Licensed under the Open Logistics Foundation License 1.3
For details on the licensing terms, see the LICENSE file.
SPDX-License-Identifier: OLFL-1.3

@author: holger leichsenring
"""


class HtmlCreator():
    '''
    Creates html fragments
    '''

    def __init__(self):
        pass

    def create_popup(self, device_name, device_value, device_time, device_state_critical, device_state_warning,
                     device_state_empty):
        '''
        Creates html based maps by specified devices
        '''

        return f'<b>{device_name}' \
            + f'</b><br>Current value: {device_value} mm' \
            + f'<br>Time: {str(device_time)}' \
            + f'<hr>Critical Threshold: {device_state_critical} mm' \
            + f'<br>Warning Threshold: {device_state_warning} mm' \
            + f'<br>Empty Threshold: {device_state_empty} mm'
