# -*- coding: utf-8 -*-
"""
Copyright Open Logistics Foundation

Licensed under the Open Logistics Foundation License 1.3
For details on the licensing terms, see the LICENSE file.
SPDX-License-Identifier: OLFL-1.3

Created on Tue Feb 11 08:53:32 2020

@author: holger leichsenring
"""
import json
from cumulocity.cumulocity_connector import CumulocityConnector
from cumulocity.managed_object_to_device_converter import ManagedObjectToDeviceConverter
from cumulocity.measurement_objects_to_measurement_converter import MeasurementObjectsToMeasurementConverter
from map_creation.folium_creator import FoliumCreator
from map_creation.html_creator import HtmlCreator
from map_creation.route_description_creator import RouteDescriptionCreator
from map_creation.travelling_salesmen_problem_solver import TravellingSalesMenProblemSolver
from open_street_map.open_street_map_connector import OpenStreetMapConnector


class MapCreator():
    '''
    Creates html based maps by specified devices
    '''

    def __init__(self):
        self.cumulocity_broker = CumulocityConnector()
        self.open_street_map_connector = OpenStreetMapConnector()
        self.travelling_sales_men_problem_solver = TravellingSalesMenProblemSolver()
        self.folium_creator = FoliumCreator()
        self.html_creator = HtmlCreator()
        self.route_description_creator = RouteDescriptionCreator()

    def read_cumulocity_data(self, date_from, date_to):
        managed_objects_devices = self.cumulocity_broker.get_managed_object_devices()
        device_converter = ManagedObjectToDeviceConverter()
        devices = device_converter.convert(managed_objects_devices)

        measurements_objects = self.cumulocity_broker.get_measurements(date_from, date_to)
        measurements_converter = MeasurementObjectsToMeasurementConverter()
        measurements = measurements_converter.convert(measurements_objects)

        return devices, measurements

    def create(self, date_from, date_to):
        '''
        Creates html based maps by specified devices
        '''
        
        devices, measurements = self.read_cumulocity_data(date_from, date_to)

        self.folium_creator \
            .create_map() \
            .define_tile_layer() \
            .create_feature_groups() \
            .create_route()

        coordinates_with_judge = []

        for device in devices:
            measurement = [m for m in measurements if m.source_id == device.id]

            has_measurement = measurement and measurement[-1] and measurement[-1].value
            value = None
            if not has_measurement:
                value = device.warning + 1
            else:
                value = measurement[-1].value

            popup_text = self.html_creator.create_popup(device.name, value, device.time, device.critical,
                                                        device.warning, device.empty)
            color, judge, feature_group = self.__get_device_state(device, value)

            self.route_description_creator.add("place", 1 - judge, device.lon, device.lat, device.time, device.empty,
                                               device.warning, device.critical, value)

            if value <= device.warning:
                coordinates_with_judge.append({
                    'judge': 1 - judge,
                    'lat': device.lat,
                    'lon': device.lon
                })
            self.folium_creator.add_marker(device.lat, device.lon, popup_text, feature_group, color)
            self.folium_creator.add_marker(device.lat, device.lon, popup_text, self.folium_creator.route, color)

        # get distance matrix to solve tsp
        coordinates_with_judge = sorted(coordinates_with_judge, key=lambda x: x['judge'])
        coordinates = [[x['lon'], x['lat']] for x in coordinates_with_judge]

        distance_matrix = self.open_street_map_connector.get_distance_matrix(coordinates)
        partition = self.travelling_sales_men_problem_solver.solve(distance_matrix)
        map_coordinates = self.get_map_coordinates(coordinates, partition)
        geo, geo_json = self.open_street_map_connector.get_geo(map_coordinates)
        self.folium_creator.geo_json(json.dumps(geo_json), self.folium_creator.route) \
            .add_markers_to_map() \
            .configure_plugins() \
            .configure_layer_control() \
                
        lats = coordinates = [x['lat'] for x in coordinates_with_judge]
        lons = coordinates = [x['lon'] for x in coordinates_with_judge]
        if lats and lons:
            min_coords = [min(lats), min(lons)]
            max_coords = [max(lats), max(lons)]
            self.folium_creator.fit_bounds(min_coords, max_coords)

        self.route_description_creator.add_way_points(partition)
        self.route_description_creator.save()
        self.folium_creator.save_map('./templates/maps/map.html')

    def get_map_coordinates(self, coordinates, partition):
        map_coordinates = []
        for x, i in enumerate(partition[:-1]):
            map_coordinates.append(coordinates[i])
        return map_coordinates

    def __get_device_state(self, device, value):
        color = None
        judge = None
        feature_group = None
        if value < device.critical:
            color = self.folium_creator.color_red
            judge = 1 - abs(device.critical - value) / device.critical
            feature_group = self.folium_creator.feature_group_red
        elif value < device.warning:
            color = self.folium_creator.color_orange
            judge = 1
            feature_group = self.folium_creator.feature_group_orange
        else:
            color = self.folium_creator.color_green
            judge = 1
            feature_group = self.folium_creator.feature_group_green
        return color, judge, feature_group
