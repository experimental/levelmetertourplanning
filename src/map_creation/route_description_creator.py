# -*- coding: utf-8 -*-
"""
Copyright Open Logistics Foundation

Licensed under the Open Logistics Foundation License 1.3
For details on the licensing terms, see the LICENSE file.
SPDX-License-Identifier: OLFL-1.3

@author: holger leichsenring
"""
import pandas as pd


class RouteDescriptionCreator():
    '''
    Creates a route description via pandas
    '''

    def __init__(self):
        self.column_waypoint_number = 'Waypoint number'
        self.column_place = 'Place'
        self.column_fill_level_evaluation = 'Fill level evaluation'
        self.column_longitude = 'Longitude'
        self.column_latitude = 'Latitude'
        self.column_retrieval_time = 'Retrieval time'
        self.column_empty_threshold = 'Empty Threshold'
        self.column_warning_threshold = 'Warning Threshold'
        self.column_critical_threshold = 'Critial Threshold'
        self.column_measure_value = 'Measure Value'
        self.route_description = pd.DataFrame({self.column_waypoint_number: pd.Series(dtype='str'),
                                               self.column_place: pd.Series(dtype='str'),
                                               self.column_fill_level_evaluation: pd.Series(dtype='str'),
                                               self.column_longitude: pd.Series(dtype='str'),
                                               self.column_latitude: pd.Series(dtype='str'),
                                               self.column_retrieval_time: pd.Series(dtype='str'),
                                               self.column_empty_threshold: pd.Series(dtype='str'),
                                               self.column_warning_threshold: pd.Series(dtype='str'),
                                               self.column_critical_threshold: pd.Series(dtype='str'),
                                               self.column_measure_value: pd.Series(dtype='str')})

    def add(self, place, judge, lon, lat, time, empty, warning, critical, value):
        new_row = {f'{self.column_place}': f'{place}', f'{self.column_fill_level_evaluation}': f'{judge}',
                   f'{self.column_longitude}': f'{lon}', f'{self.column_latitude}': f'{lat}',
                   f'{self.column_retrieval_time}': f'{time}', f'{self.column_empty_threshold}': f'{empty}',
                   f'{self.column_warning_threshold}': f'{warning}', f'{self.column_critical_threshold}': f'{critical}',
                   f'{self.column_measure_value}': f'{value}'}
        self.route_description = pd.concat([self.route_description, pd.DataFrame([new_row])], ignore_index=True)
        return self

    def add_way_points(self, partition):
        for x, i in enumerate(partition[:-1]):
            self.route_description.loc[f'{i}', self.column_waypoint_number] = f'{x}'

    def save(self):
        # self.route_description.to_excel('./downloads/waypoint.xlsx', index=False)
        return
