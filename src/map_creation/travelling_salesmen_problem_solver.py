# -*- coding: utf-8 -*-
"""
Copyright Open Logistics Foundation

Licensed under the Open Logistics Foundation License 1.3
For details on the licensing terms, see the LICENSE file.
SPDX-License-Identifier: OLFL-1.3

@author: holger leichsenring
"""
import ast
from ortools.constraint_solver import pywrapcp
from ortools.constraint_solver import routing_enums_pb2


class TravellingSalesMenProblemSolver:
    def __init__(self):
        self.manager = None
        self.data = None
        pass

    def solve(self, coordinates):
        distance_matrix = ast.literal_eval(coordinates)

        # prepare tsp solver
        self.data = {}
        self.data['distance_matrix'] = distance_matrix['durations']  # yapf: disable
        self.data['num_vehicles'] = 1
        self.data['depot'] = 0

        # Create the routing index manager.
        self.manager = pywrapcp.RoutingIndexManager(
            len(self.data['distance_matrix']), self.data['num_vehicles'], self.data['depot'])

        # Create Routing Model.
        routing = pywrapcp.RoutingModel(self.manager)

        transit_callback_index = routing.RegisterTransitCallback(self.distance_callback)

        # Define cost of each arc.
        routing.SetArcCostEvaluatorOfAllVehicles(transit_callback_index)

        # Setting first solution heuristic.
        search_parameters = pywrapcp.DefaultRoutingSearchParameters()
        search_parameters.first_solution_strategy = (
            routing_enums_pb2.FirstSolutionStrategy.PATH_CHEAPEST_ARC)

        # Solve the problem.
        assignment = routing.SolveWithParameters(search_parameters)

        if assignment:
            index = routing.Start(0)
            route_distance = 0
            partition = []
            while not routing.IsEnd(index):
                partition.append(self.manager.IndexToNode(index))
                previous_index = index
                index = assignment.Value(routing.NextVar(index))
                route_distance += routing.GetArcCostForVehicle(previous_index, index, 0)
            partition.append(self.manager.IndexToNode(index))
            return partition

        return None

    def distance_callback(self, from_index, to_index):
        # Convert from routing variable Index to distance matrix NodeIndex.
        from_node = self.manager.IndexToNode(from_index)
        to_node = self.manager.IndexToNode(to_index)
        return self.data['distance_matrix'][from_node][to_node]
