import dateutil.parser
from dataclasses import dataclass
from datetime import datetime
from typing import List, Any, Optional, TypeVar, Callable, Type, cast

T = TypeVar("T")


def from_list(f: Callable[[Any], T], x: Any) -> List[T]:
    # assert isinstance(x, list)
    return [f(y) for y in x]


def from_none(x: Any) -> Any:
    # assert x is None
    return x


def from_union(fs, x):
    for f in fs:
        try:
            return f(x)
        except:
            pass
    # assert False


def from_str(x: Any) -> str:
    # assert isinstance(x, str)
    return x


def from_int(x: Any) -> int:
    # assert isinstance(x, int) and not isinstance(x, bool)
    return x


def from_float(x: Any) -> float:
    # assert isinstance(x, (float, int)) and not isinstance(x, bool)
    return float(x)


def to_float(x: Any) -> float:
    # assert isinstance(x, float)
    return x


def to_class(c: Type[T], x: Any) -> dict:
    # assert isinstance(x, c)
    return cast(Any, x).to_dict()


def from_bool(x: Any) -> bool:
    # assert isinstance(x, bool)
    return x


def from_datetime(x: Any) -> datetime:
    return dateutil.parser.parse(x)


def is_type(t: Type[T], x: Any) -> T:
    # assert isinstance(x, t)
    return x


@dataclass
class AdditionParents:
    references: Optional[List[Any]] = None
    addition_parents_self: Optional[str] = None

    @staticmethod
    def from_dict(obj: Any) -> 'AdditionParents':
        # assert isinstance(obj, dict)
        references = from_union([lambda x: from_list(lambda x: x, x), from_none], obj.get("references"))
        addition_parents_self = from_union([from_str, from_none], obj.get("self"))
        return AdditionParents(references, addition_parents_self)

    def to_dict(self) -> dict:
        result: dict = {}
        if self.references is not None:
            result["references"] = from_union([lambda x: from_list(lambda x: x, x), from_none], self.references)
        if self.addition_parents_self is not None:
            result["self"] = from_union([from_str, from_none], self.addition_parents_self)
        return result


@dataclass
class C8YActiveAlarmsStatus:
    critical: Optional[int] = None

    @staticmethod
    def from_dict(obj: Any) -> 'C8YActiveAlarmsStatus':
        # assert isinstance(obj, dict)
        critical = from_union([from_int, from_none], obj.get("critical"))
        return C8YActiveAlarmsStatus(critical)

    def to_dict(self) -> dict:
        result: dict = {}
        if self.critical is not None:
            result["critical"] = from_union([from_int, from_none], self.critical)
        return result


@dataclass
class C8YFirmware:
    version: Optional[str] = None

    @staticmethod
    def from_dict(obj: Any) -> 'C8YFirmware':
        # assert isinstance(obj, dict)
        version = from_union([from_str, from_none], obj.get("version"))
        return C8YFirmware(version)

    def to_dict(self) -> dict:
        result: dict = {}
        if self.version is not None:
            result["version"] = from_union([from_str, from_none], self.version)
        return result


@dataclass
class C8YHardware:
    serial_number: Optional[str] = None
    model: Optional[str] = None
    revision: Optional[str] = None

    @staticmethod
    def from_dict(obj: Any) -> 'C8YHardware':
        # assert isinstance(obj, dict)
        serial_number = from_union([from_str, from_none], obj.get("serialNumber"))
        model = from_union([from_str, from_none], obj.get("model"))
        revision = from_union([from_str, from_none], obj.get("revision"))
        return C8YHardware(serial_number, model, revision)

    def to_dict(self) -> dict:
        result: dict = {}
        if self.serial_number is not None:
            result["serialNumber"] = from_union([from_str, from_none], self.serial_number)
        if self.model is not None:
            result["model"] = from_union([from_str, from_none], self.model)
        if self.revision is not None:
            result["revision"] = from_union([from_str, from_none], self.revision)
        return result


@dataclass
class C8YIsDevice:
    pass

    @staticmethod
    def from_dict(obj: Any) -> 'C8YIsDevice':
        # assert isinstance(obj, dict)
        return C8YIsDevice()

    def to_dict(self) -> dict:
        result: dict = {}
        return result


@dataclass
class C8YMobile:
    iccid: Optional[str] = None

    @staticmethod
    def from_dict(obj: Any) -> 'C8YMobile':
        # assert isinstance(obj, dict)
        iccid = from_union([from_str, from_none], obj.get("iccid"))
        return C8YMobile(iccid)

    def to_dict(self) -> dict:
        result: dict = {}
        if self.iccid is not None:
            result["iccid"] = from_union([from_str, from_none], self.iccid)
        return result


@dataclass
class C8YPosition:
    tracking_protocol: Optional[str] = None
    lng: Optional[float] = None
    alt: Optional[int] = None
    accuracy: Optional[int] = None
    time: Optional[str] = None
    lat: Optional[float] = None
    report_reason: Optional[str] = None

    @staticmethod
    def from_dict(obj: Any) -> 'C8YPosition':
        # assert isinstance(obj, dict)
        tracking_protocol = from_union([from_str, from_none], obj.get("trackingProtocol"))
        lng = from_union([from_float, from_none], obj.get("lng"))
        alt = from_union([from_int, from_none], obj.get("alt"))
        accuracy = from_union([from_int, from_none], obj.get("accuracy"))
        time = from_union([from_str, from_none], obj.get("time"))
        lat = from_union([from_float, from_none], obj.get("lat"))
        report_reason = from_union([from_str, from_none], obj.get("reportReason"))
        return C8YPosition(tracking_protocol, lng, alt, accuracy, time, lat, report_reason)

    def to_dict(self) -> dict:
        result: dict = {}
        if self.tracking_protocol is not None:
            result["trackingProtocol"] = from_union([from_str, from_none], self.tracking_protocol)
        if self.lng is not None:
            result["lng"] = from_union([to_float, from_none], self.lng)
        if self.alt is not None:
            result["alt"] = from_union([from_int, from_none], self.alt)
        if self.accuracy is not None:
            result["accuracy"] = from_union([from_int, from_none], self.accuracy)
        if self.time is not None:
            result["time"] = from_union([from_str, from_none], self.time)
        if self.lat is not None:
            result["lat"] = from_union([to_float, from_none], self.lat)
        if self.report_reason is not None:
            result["reportReason"] = from_union([from_str, from_none], self.report_reason)
        return result


@dataclass
class LastEnt:
    eta: None
    id: Optional[int] = None

    @staticmethod
    def from_dict(obj: Any) -> 'LastEnt':
        # assert isinstance(obj, dict)
        eta = from_none(obj.get("eta"))
        id = from_union([from_int, from_none], obj.get("id"))
        return LastEnt(eta, id)

    def to_dict(self) -> dict:
        result: dict = {}
        if self.eta is not None:
            result["eta"] = from_none(self.eta)
        if self.id is not None:
            result["id"] = from_union([from_int, from_none], self.id)
        return result


@dataclass
class ToilEvents:
    last_measurement: Optional[LastEnt] = None
    last_event: Optional[LastEnt] = None

    @staticmethod
    def from_dict(obj: Any) -> 'ToilEvents':
        # assert isinstance(obj, dict)
        last_measurement = from_union([LastEnt.from_dict, from_none], obj.get("lastMeasurement"))
        last_event = from_union([LastEnt.from_dict, from_none], obj.get("lastEvent"))
        return ToilEvents(last_measurement, last_event)

    def to_dict(self) -> dict:
        result: dict = {}
        if self.last_measurement is not None:
            result["lastMeasurement"] = from_union([lambda x: to_class(LastEnt, x), from_none], self.last_measurement)
        if self.last_event is not None:
            result["lastEvent"] = from_union([lambda x: to_class(LastEnt, x), from_none], self.last_event)
        return result


@dataclass
class Parameters:
    motion_detection_hysteresis_upper_bound: Optional[int] = None
    temperature_threshold_trigger_active: Optional[bool] = None
    mqttsn_ip: Optional[str] = None
    mqttsn_connect_duration_sec: Optional[int] = None
    distance_hysteresis_threshold: Optional[int] = None
    distance_warning_threshold: Optional[int] = None
    zero_pos_axis_acceleration_mg_y: Optional[int] = None
    distance_sensor_interval_s: Optional[int] = None
    modem_socket_open_timeout_ms: Optional[int] = None
    zero_pos_axis_acceleration_mg_z: Optional[int] = None
    motion_detection_hysteresis_lower_bound: Optional[int] = None
    zero_pos_axis_acceleration_mg_x: Optional[int] = None
    temperature_max_threshold: Optional[int] = None
    device_communication_session_try_count: Optional[int] = None
    motion_detection_hysteresis_maximum_count: Optional[int] = None
    distance_sensor_measurement_start_time: Optional[int] = None
    modem_connect_timeout_ms: Optional[int] = None
    distance_empty_threshold: Optional[int] = None
    accelerometer_motion_threshold: Optional[int] = None
    mqttsn_port: Optional[int] = None
    temperature_min_threshold: Optional[int] = None
    regular_transmission_interval: Optional[int] = None
    mqttsn_command_timeout_ms: Optional[int] = None
    distance_sensor_times: Optional[List[int]] = None
    motion_detection_sensor_inverval_s: Optional[int] = None
    distance_critical_threshold: Optional[int] = None
    communication_sequence_energy_consumption_uah: Optional[int] = None
    distance_sensor_measurement_stop_time: Optional[int] = None
    mqttsn_receive_wating_ms: Optional[int] = None

    @staticmethod
    def from_dict(obj: Any) -> 'Parameters':
        # assert isinstance(obj, dict)
        motion_detection_hysteresis_upper_bound = from_union([from_int, from_none],
                                                             obj.get("motionDetectionHysteresisUpperBound"))
        temperature_threshold_trigger_active = from_union([from_bool, from_none],
                                                          obj.get("temperatureThresholdTriggerActive"))
        mqttsn_ip = from_union([from_str, from_none], obj.get("mqttsnIp"))
        mqttsn_connect_duration_sec = from_union([from_int, from_none], obj.get("mqttsnConnectDurationSec"))
        distance_hysteresis_threshold = from_union([from_int, from_none], obj.get("distanceHysteresisThreshold"))
        distance_warning_threshold = from_union([from_int, from_none], obj.get("distanceWarningThreshold"))
        zero_pos_axis_acceleration_mg_y = from_union([from_int, from_none], obj.get("zeroPosAxisAccelerationMgY"))
        distance_sensor_interval_s = from_union([from_int, from_none], obj.get("distanceSensorIntervalS"))
        modem_socket_open_timeout_ms = from_union([from_int, from_none], obj.get("modemSocketOpenTimeoutMs"))
        zero_pos_axis_acceleration_mg_z = from_union([from_int, from_none], obj.get("zeroPosAxisAccelerationMgZ"))
        motion_detection_hysteresis_lower_bound = from_union([from_int, from_none],
                                                             obj.get("motionDetectionHysteresisLowerBound"))
        zero_pos_axis_acceleration_mg_x = from_union([from_int, from_none], obj.get("zeroPosAxisAccelerationMgX"))
        temperature_max_threshold = from_union([from_int, from_none], obj.get("temperatureMaxThreshold"))
        device_communication_session_try_count = from_union([from_int, from_none],
                                                            obj.get("deviceCommunicationSessionTryCount"))
        motion_detection_hysteresis_maximum_count = from_union([from_int, from_none],
                                                               obj.get("motionDetectionHysteresisMaximumCount"))
        distance_sensor_measurement_start_time = from_union([from_int, from_none],
                                                            obj.get("distanceSensorMeasurementStartTime"))
        modem_connect_timeout_ms = from_union([from_int, from_none], obj.get("modemConnectTimeoutMs"))
        distance_empty_threshold = from_union([from_int, from_none], obj.get("distanceEmptyThreshold"))
        accelerometer_motion_threshold = from_union([from_int, from_none], obj.get("accelerometerMotionThreshold"))
        mqttsn_port = from_union([from_int, from_none], obj.get("mqttsnPort"))
        temperature_min_threshold = from_union([from_int, from_none], obj.get("temperatureMinThreshold"))
        regular_transmission_interval = from_union([from_int, from_none], obj.get("regularTransmissionInterval"))
        mqttsn_command_timeout_ms = from_union([from_int, from_none], obj.get("mqttsnCommandTimeoutMs"))
        distance_sensor_times = from_union([lambda x: from_list(from_int, x), from_none],
                                           obj.get("distanceSensorTimes"))
        motion_detection_sensor_inverval_s = from_union([from_int, from_none],
                                                        obj.get("motionDetectionSensorInvervalS"))
        distance_critical_threshold = from_union([from_int, from_none], obj.get("distanceCriticalThreshold"))
        communication_sequence_energy_consumption_uah = from_union([from_int, from_none],
                                                                   obj.get("communicationSequenceEnergyConsumptionUah"))
        distance_sensor_measurement_stop_time = from_union([from_int, from_none],
                                                           obj.get("distanceSensorMeasurementStopTime"))
        mqttsn_receive_wating_ms = from_union([from_int, from_none], obj.get("mqttsnReceiveWatingMs"))
        return Parameters(motion_detection_hysteresis_upper_bound, temperature_threshold_trigger_active, mqttsn_ip,
                          mqttsn_connect_duration_sec, distance_hysteresis_threshold, distance_warning_threshold,
                          zero_pos_axis_acceleration_mg_y, distance_sensor_interval_s, modem_socket_open_timeout_ms,
                          zero_pos_axis_acceleration_mg_z, motion_detection_hysteresis_lower_bound,
                          zero_pos_axis_acceleration_mg_x, temperature_max_threshold,
                          device_communication_session_try_count, motion_detection_hysteresis_maximum_count,
                          distance_sensor_measurement_start_time, modem_connect_timeout_ms, distance_empty_threshold,
                          accelerometer_motion_threshold, mqttsn_port, temperature_min_threshold,
                          regular_transmission_interval, mqttsn_command_timeout_ms, distance_sensor_times,
                          motion_detection_sensor_inverval_s, distance_critical_threshold,
                          communication_sequence_energy_consumption_uah, distance_sensor_measurement_stop_time,
                          mqttsn_receive_wating_ms)

    def to_dict(self) -> dict:
        result: dict = {}
        if self.motion_detection_hysteresis_upper_bound is not None:
            result["motionDetectionHysteresisUpperBound"] = from_union([from_int, from_none],
                                                                       self.motion_detection_hysteresis_upper_bound)
        if self.temperature_threshold_trigger_active is not None:
            result["temperatureThresholdTriggerActive"] = from_union([from_bool, from_none],
                                                                     self.temperature_threshold_trigger_active)
        if self.mqttsn_ip is not None:
            result["mqttsnIp"] = from_union([from_str, from_none], self.mqttsn_ip)
        if self.mqttsn_connect_duration_sec is not None:
            result["mqttsnConnectDurationSec"] = from_union([from_int, from_none], self.mqttsn_connect_duration_sec)
        if self.distance_hysteresis_threshold is not None:
            result["distanceHysteresisThreshold"] = from_union([from_int, from_none],
                                                               self.distance_hysteresis_threshold)
        if self.distance_warning_threshold is not None:
            result["distanceWarningThreshold"] = from_union([from_int, from_none], self.distance_warning_threshold)
        if self.zero_pos_axis_acceleration_mg_y is not None:
            result["zeroPosAxisAccelerationMgY"] = from_union([from_int, from_none],
                                                              self.zero_pos_axis_acceleration_mg_y)
        if self.distance_sensor_interval_s is not None:
            result["distanceSensorIntervalS"] = from_union([from_int, from_none], self.distance_sensor_interval_s)
        if self.modem_socket_open_timeout_ms is not None:
            result["modemSocketOpenTimeoutMs"] = from_union([from_int, from_none], self.modem_socket_open_timeout_ms)
        if self.zero_pos_axis_acceleration_mg_z is not None:
            result["zeroPosAxisAccelerationMgZ"] = from_union([from_int, from_none],
                                                              self.zero_pos_axis_acceleration_mg_z)
        if self.motion_detection_hysteresis_lower_bound is not None:
            result["motionDetectionHysteresisLowerBound"] = from_union([from_int, from_none],
                                                                       self.motion_detection_hysteresis_lower_bound)
        if self.zero_pos_axis_acceleration_mg_x is not None:
            result["zeroPosAxisAccelerationMgX"] = from_union([from_int, from_none],
                                                              self.zero_pos_axis_acceleration_mg_x)
        if self.temperature_max_threshold is not None:
            result["temperatureMaxThreshold"] = from_union([from_int, from_none], self.temperature_max_threshold)
        if self.device_communication_session_try_count is not None:
            result["deviceCommunicationSessionTryCount"] = from_union([from_int, from_none],
                                                                      self.device_communication_session_try_count)
        if self.motion_detection_hysteresis_maximum_count is not None:
            result["motionDetectionHysteresisMaximumCount"] = from_union([from_int, from_none],
                                                                         self.motion_detection_hysteresis_maximum_count)
        if self.distance_sensor_measurement_start_time is not None:
            result["distanceSensorMeasurementStartTime"] = from_union([from_int, from_none],
                                                                      self.distance_sensor_measurement_start_time)
        if self.modem_connect_timeout_ms is not None:
            result["modemConnectTimeoutMs"] = from_union([from_int, from_none], self.modem_connect_timeout_ms)
        if self.distance_empty_threshold is not None:
            result["distanceEmptyThreshold"] = from_union([from_int, from_none], self.distance_empty_threshold)
        if self.accelerometer_motion_threshold is not None:
            result["accelerometerMotionThreshold"] = from_union([from_int, from_none],
                                                                self.accelerometer_motion_threshold)
        if self.mqttsn_port is not None:
            result["mqttsnPort"] = from_union([from_int, from_none], self.mqttsn_port)
        if self.temperature_min_threshold is not None:
            result["temperatureMinThreshold"] = from_union([from_int, from_none], self.temperature_min_threshold)
        if self.regular_transmission_interval is not None:
            result["regularTransmissionInterval"] = from_union([from_int, from_none],
                                                               self.regular_transmission_interval)
        if self.mqttsn_command_timeout_ms is not None:
            result["mqttsnCommandTimeoutMs"] = from_union([from_int, from_none], self.mqttsn_command_timeout_ms)
        if self.distance_sensor_times is not None:
            result["distanceSensorTimes"] = from_union([lambda x: from_list(from_int, x), from_none],
                                                       self.distance_sensor_times)
        if self.motion_detection_sensor_inverval_s is not None:
            result["motionDetectionSensorInvervalS"] = from_union([from_int, from_none],
                                                                  self.motion_detection_sensor_inverval_s)
        if self.distance_critical_threshold is not None:
            result["distanceCriticalThreshold"] = from_union([from_int, from_none], self.distance_critical_threshold)
        if self.communication_sequence_energy_consumption_uah is not None:
            result["communicationSequenceEnergyConsumptionUah"] = from_union([from_int, from_none],
                                                                             self.communication_sequence_energy_consumption_uah)
        if self.distance_sensor_measurement_stop_time is not None:
            result["distanceSensorMeasurementStopTime"] = from_union([from_int, from_none],
                                                                     self.distance_sensor_measurement_stop_time)
        if self.mqttsn_receive_wating_ms is not None:
            result["mqttsnReceiveWatingMs"] = from_union([from_int, from_none], self.mqttsn_receive_wating_ms)
        return result


@dataclass
class Configuration:
    owner: Optional[str] = None
    creation_time: Optional[datetime] = None
    id: Optional[int] = None
    parameters: Optional[Parameters] = None

    @staticmethod
    def from_dict(obj: Any) -> 'Configuration':
        # assert isinstance(obj, dict)
        owner = from_union([from_str, from_none], obj.get("owner"))
        creation_time = from_union([from_datetime, from_none], obj.get("creationTime"))
        id = from_union([from_int, from_none], obj.get("id"))
        parameters = from_union([Parameters.from_dict, from_none], obj.get("parameters"))
        return Configuration(owner, creation_time, id, parameters)

    def to_dict(self) -> dict:
        result: dict = {}
        if self.owner is not None:
            result["owner"] = from_union([from_str, from_none], self.owner)
        if self.creation_time is not None:
            result["creationTime"] = from_union([lambda x: x.isoformat(), from_none], self.creation_time)
        if self.id is not None:
            result["id"] = from_union([from_int, from_none], self.id)
        if self.parameters is not None:
            result["parameters"] = from_union([lambda x: to_class(Parameters, x), from_none], self.parameters)
        return result


@dataclass
class ToilRemoteConfiguration:
    last_device_configuration_sync_time: Optional[datetime] = None
    configurations: Optional[List[Configuration]] = None
    device_configuration: Optional[Configuration] = None
    sent_device_configuration: Optional[Configuration] = None
    default_configuration: Optional[Configuration] = None

    @staticmethod
    def from_dict(obj: Any) -> 'ToilRemoteConfiguration':
        # assert isinstance(obj, dict)
        last_device_configuration_sync_time = from_union([from_datetime, from_none],
                                                         obj.get("lastDeviceConfigurationSyncTime"))
        configurations = from_union([lambda x: from_list(Configuration.from_dict, x), from_none],
                                    obj.get("configurations"))
        device_configuration = from_union([Configuration.from_dict, from_none], obj.get("deviceConfiguration"))
        sent_device_configuration = from_union([Configuration.from_dict, from_none], obj.get("sentDeviceConfiguration"))
        default_configuration = from_union([Configuration.from_dict, from_none], obj.get("defaultConfiguration"))
        return ToilRemoteConfiguration(last_device_configuration_sync_time, configurations, device_configuration,
                                       sent_device_configuration, default_configuration)

    def to_dict(self) -> dict:
        result: dict = {}
        if self.last_device_configuration_sync_time is not None:
            result["lastDeviceConfigurationSyncTime"] = from_union([lambda x: x.isoformat(), from_none],
                                                                   self.last_device_configuration_sync_time)
        if self.configurations is not None:
            result["configurations"] = from_union(
                [lambda x: from_list(lambda x: to_class(Configuration, x), x), from_none], self.configurations)
        if self.device_configuration is not None:
            result["deviceConfiguration"] = from_union([lambda x: to_class(Configuration, x), from_none],
                                                       self.device_configuration)
        if self.sent_device_configuration is not None:
            result["sentDeviceConfiguration"] = from_union([lambda x: to_class(Configuration, x), from_none],
                                                           self.sent_device_configuration)
        if self.default_configuration is not None:
            result["defaultConfiguration"] = from_union([lambda x: to_class(Configuration, x), from_none],
                                                        self.default_configuration)
        return result


@dataclass
class ManagedObject:
    id: Optional[int] = None
    addition_parents: Optional[AdditionParents] = None
    owner: Optional[str] = None
    child_devices: Optional[AdditionParents] = None
    child_assets: Optional[AdditionParents] = None
    creation_time: Optional[datetime] = None
    type: Optional[str] = None
    last_updated: Optional[datetime] = None
    child_additions: Optional[AdditionParents] = None
    name: Optional[str] = None
    device_parents: Optional[AdditionParents] = None
    asset_parents: Optional[AdditionParents] = None
    managed_object_self: Optional[str] = None
    c8_y_firmware: Optional[C8YFirmware] = None
    com_cumulocity_model_agent: Optional[C8YIsDevice] = None
    c8_y_supported_operations: Optional[List[str]] = None
    c8_y_position: Optional[C8YPosition] = None
    c8_y_hardware: Optional[C8YHardware] = None
    toil_container_id: Optional[str] = None
    c8_y_mobile: Optional[C8YMobile] = None
    toil_configuration: Optional[C8YIsDevice] = None
    c8_y_active_alarms_status: Optional[C8YActiveAlarmsStatus] = None
    c8_y_is_device: Optional[C8YIsDevice] = None
    toil_remote_configuration: Optional[ToilRemoteConfiguration] = None
    toil_hierarchy: Optional[str] = None
    toil_events: Optional[ToilEvents] = None

    @staticmethod
    def from_dict(obj: Any) -> 'ManagedObject':
        # assert isinstance(obj, dict)
        id = from_union([from_none, lambda x: int(from_str(x))], obj.get("id"))
        addition_parents = from_union([AdditionParents.from_dict, from_none], obj.get("additionParents"))
        owner = from_union([from_str, from_none], obj.get("owner"))
        child_devices = from_union([AdditionParents.from_dict, from_none], obj.get("childDevices"))
        child_assets = from_union([AdditionParents.from_dict, from_none], obj.get("childAssets"))
        creation_time = from_union([from_datetime, from_none], obj.get("creationTime"))
        type = from_union([from_str, from_none], obj.get("type"))
        last_updated = from_union([from_datetime, from_none], obj.get("lastUpdated"))
        child_additions = from_union([AdditionParents.from_dict, from_none], obj.get("childAdditions"))
        name = from_union([from_str, from_none], obj.get("name"))
        device_parents = from_union([AdditionParents.from_dict, from_none], obj.get("deviceParents"))
        asset_parents = from_union([AdditionParents.from_dict, from_none], obj.get("assetParents"))
        managed_object_self = from_union([from_str, from_none], obj.get("self"))
        c8_y_firmware = from_union([C8YFirmware.from_dict, from_none], obj.get("c8y_Firmware"))
        com_cumulocity_model_agent = from_union([C8YIsDevice.from_dict, from_none],
                                                obj.get("com_cumulocity_model_Agent"))
        c8_y_supported_operations = from_union([lambda x: from_list(from_str, x), from_none],
                                               obj.get("c8y_SupportedOperations"))
        c8_y_position = from_union([C8YPosition.from_dict, from_none], obj.get("c8y_Position"))
        c8_y_hardware = from_union([C8YHardware.from_dict, from_none], obj.get("c8y_Hardware"))
        toil_container_id = from_union([from_str, from_none], obj.get("toil_ContainerId"))
        c8_y_mobile = from_union([C8YMobile.from_dict, from_none], obj.get("c8y_Mobile"))
        toil_configuration = from_union([C8YIsDevice.from_dict, from_none], obj.get("toil_Configuration"))
        c8_y_active_alarms_status = from_union([C8YActiveAlarmsStatus.from_dict, from_none],
                                               obj.get("c8y_ActiveAlarmsStatus"))
        c8_y_is_device = from_union([C8YIsDevice.from_dict, from_none], obj.get("c8y_IsDevice"))
        toil_remote_configuration = from_union([ToilRemoteConfiguration.from_dict, from_none],
                                               obj.get("toil_RemoteConfiguration"))
        toil_hierarchy = from_union([from_str, from_none], obj.get("toil_Hierarchy"))
        toil_events = from_union([ToilEvents.from_dict, from_none], obj.get("toil_Events"))
        return ManagedObject(id, addition_parents, owner, child_devices, child_assets, creation_time, type,
                             last_updated, child_additions, name, device_parents, asset_parents, managed_object_self,
                             c8_y_firmware, com_cumulocity_model_agent, c8_y_supported_operations, c8_y_position,
                             c8_y_hardware, toil_container_id, c8_y_mobile, toil_configuration,
                             c8_y_active_alarms_status, c8_y_is_device, toil_remote_configuration, toil_hierarchy,
                             toil_events)

    def to_dict(self) -> dict:
        result: dict = {}
        if self.id is not None:
            result["id"] = from_union([lambda x: from_none((lambda x: is_type(type(None), x))(x)),
                                       lambda x: from_str((lambda x: str((lambda x: is_type(int, x))(x)))(x))], self.id)
        if self.addition_parents is not None:
            result["additionParents"] = from_union([lambda x: to_class(AdditionParents, x), from_none],
                                                   self.addition_parents)
        if self.owner is not None:
            result["owner"] = from_union([from_str, from_none], self.owner)
        if self.child_devices is not None:
            result["childDevices"] = from_union([lambda x: to_class(AdditionParents, x), from_none], self.child_devices)
        if self.child_assets is not None:
            result["childAssets"] = from_union([lambda x: to_class(AdditionParents, x), from_none], self.child_assets)
        if self.creation_time is not None:
            result["creationTime"] = from_union([lambda x: x.isoformat(), from_none], self.creation_time)
        if self.type is not None:
            result["type"] = from_union([from_str, from_none], self.type)
        if self.last_updated is not None:
            result["lastUpdated"] = from_union([lambda x: x.isoformat(), from_none], self.last_updated)
        if self.child_additions is not None:
            result["childAdditions"] = from_union([lambda x: to_class(AdditionParents, x), from_none],
                                                  self.child_additions)
        if self.name is not None:
            result["name"] = from_union([from_str, from_none], self.name)
        if self.device_parents is not None:
            result["deviceParents"] = from_union([lambda x: to_class(AdditionParents, x), from_none],
                                                 self.device_parents)
        if self.asset_parents is not None:
            result["assetParents"] = from_union([lambda x: to_class(AdditionParents, x), from_none], self.asset_parents)
        if self.managed_object_self is not None:
            result["self"] = from_union([from_str, from_none], self.managed_object_self)
        if self.c8_y_firmware is not None:
            result["c8y_Firmware"] = from_union([lambda x: to_class(C8YFirmware, x), from_none], self.c8_y_firmware)
        if self.com_cumulocity_model_agent is not None:
            result["com_cumulocity_model_Agent"] = from_union([lambda x: to_class(C8YIsDevice, x), from_none],
                                                              self.com_cumulocity_model_agent)
        if self.c8_y_supported_operations is not None:
            result["c8y_SupportedOperations"] = from_union([lambda x: from_list(from_str, x), from_none],
                                                           self.c8_y_supported_operations)
        if self.c8_y_position is not None:
            result["c8y_Position"] = from_union([lambda x: to_class(C8YPosition, x), from_none], self.c8_y_position)
        if self.c8_y_hardware is not None:
            result["c8y_Hardware"] = from_union([lambda x: to_class(C8YHardware, x), from_none], self.c8_y_hardware)
        if self.toil_container_id is not None:
            result["toil_ContainerId"] = from_union([from_str, from_none], self.toil_container_id)
        if self.c8_y_mobile is not None:
            result["c8y_Mobile"] = from_union([lambda x: to_class(C8YMobile, x), from_none], self.c8_y_mobile)
        if self.toil_configuration is not None:
            result["toil_Configuration"] = from_union([lambda x: to_class(C8YIsDevice, x), from_none],
                                                      self.toil_configuration)
        if self.c8_y_active_alarms_status is not None:
            result["c8y_ActiveAlarmsStatus"] = from_union([lambda x: to_class(C8YActiveAlarmsStatus, x), from_none],
                                                          self.c8_y_active_alarms_status)
        if self.c8_y_is_device is not None:
            result["c8y_IsDevice"] = from_union([lambda x: to_class(C8YIsDevice, x), from_none], self.c8_y_is_device)
        if self.toil_remote_configuration is not None:
            result["toil_RemoteConfiguration"] = from_union([lambda x: to_class(ToilRemoteConfiguration, x), from_none],
                                                            self.toil_remote_configuration)
        if self.toil_hierarchy is not None:
            result["toil_Hierarchy"] = from_union([from_str, from_none], self.toil_hierarchy)
        if self.toil_events is not None:
            result["toil_Events"] = from_union([lambda x: to_class(ToilEvents, x), from_none], self.toil_events)
        return result


@dataclass
class Statistics:
    page_size: Optional[int] = None
    current_page: Optional[int] = None

    @staticmethod
    def from_dict(obj: Any) -> 'Statistics':
        # assert isinstance(obj, dict)
        page_size = from_union([from_int, from_none], obj.get("pageSize"))
        current_page = from_union([from_int, from_none], obj.get("currentPage"))
        return Statistics(page_size, current_page)

    def to_dict(self) -> dict:
        result: dict = {}
        if self.page_size is not None:
            result["pageSize"] = from_union([from_int, from_none], self.page_size)
        if self.current_page is not None:
            result["currentPage"] = from_union([from_int, from_none], self.current_page)
        return result


@dataclass
class DevicePage:
    next: Optional[str] = None
    page_self: Optional[str] = None
    managed_objects: Optional[List[ManagedObject]] = None
    statistics: Optional[Statistics] = None

    @staticmethod
    def from_dict(obj: Any) -> 'DevicePage':
        # assert isinstance(obj, dict)
        next = from_union([from_str, from_none], obj.get("next"))
        page_self = from_union([from_str, from_none], obj.get("self"))
        managed_objects = from_union([lambda x: from_list(ManagedObject.from_dict, x), from_none],
                                     obj.get("managedObjects"))
        statistics = from_union([Statistics.from_dict, from_none], obj.get("statistics"))
        return DevicePage(next, page_self, managed_objects, statistics)

    def to_dict(self) -> dict:
        result: dict = {}
        if self.next is not None:
            result["next"] = from_union([from_str, from_none], self.next)
        if self.temperatures_self is not None:
            result["self"] = from_union([from_str, from_none], self.temperatures_self)
        if self.managed_objects is not None:
            result["managedObjects"] = from_union(
                [lambda x: from_list(lambda x: to_class(ManagedObject, x), x), from_none], self.managed_objects)
        if self.statistics is not None:
            result["statistics"] = from_union([lambda x: to_class(Statistics, x), from_none], self.statistics)
        return result


def device_page_from_dict(s: Any) -> DevicePage:
    return DevicePage.from_dict(s)


def device_page_to_dict(x: DevicePage) -> Any:
    return to_class(DevicePage, x)
