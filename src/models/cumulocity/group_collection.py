import dateutil.parser
from dataclasses import dataclass
from datetime import datetime
from typing import Optional, Any, List, TypeVar, Type, cast, Callable

T = TypeVar("T")


def from_none(x: Any) -> Any:
    # assert x is None
    return x


def from_str(x: Any) -> str:
    # assert isinstance(x, str)
    return x


def from_union(fs, x):
    for f in fs:
        try:
            return f(x)
        except:
            pass
    # assert False


def is_type(t: Type[T], x: Any) -> T:
    # assert isinstance(x, t)
    return x


def to_class(c: Type[T], x: Any) -> dict:
    # assert isinstance(x, c)
    return cast(Any, x).to_dict()


def from_list(f: Callable[[Any], T], x: Any) -> List[T]:
    # assert isinstance(x, list)
    return [f(y) for y in x]


def from_bool(x: Any) -> bool:
    # assert isinstance(x, bool)
    return x


def from_datetime(x: Any) -> datetime:
    return dateutil.parser.parse(x)


def from_int(x: Any) -> int:
    # assert isinstance(x, int) and not isinstance(x, bool)
    return x


@dataclass
class ReferenceManagedObject:
    id: Optional[int] = None
    managed_object_self: Optional[str] = None
    name: Optional[str] = None

    @staticmethod
    def from_dict(obj: Any) -> 'ReferenceManagedObject':
        # assert isinstance(obj, dict)
        id = from_union([from_none, lambda x: int(from_str(x))], obj.get("id"))
        managed_object_self = from_union([from_str, from_none], obj.get("self"))
        name = from_union([from_str, from_none], obj.get("name"))
        return ReferenceManagedObject(id, managed_object_self, name)

    def to_dict(self) -> dict:
        result: dict = {}
        if self.id is not None:
            result["id"] = from_union([lambda x: from_none((lambda x: is_type(type(None), x))(x)),
                                       lambda x: from_str((lambda x: str((lambda x: is_type(int, x))(x)))(x))], self.id)
        if self.managed_object_self is not None:
            result["self"] = from_union([from_str, from_none], self.managed_object_self)
        if self.name is not None:
            result["name"] = from_union([from_str, from_none], self.name)
        return result


@dataclass
class Reference:
    managed_object: Optional[ReferenceManagedObject] = None
    reference_self: Optional[str] = None

    @staticmethod
    def from_dict(obj: Any) -> 'Reference':
        # assert isinstance(obj, dict)
        managed_object = from_union([ReferenceManagedObject.from_dict, from_none], obj.get("managedObject"))
        reference_self = from_union([from_str, from_none], obj.get("self"))
        return Reference(managed_object, reference_self)

    def to_dict(self) -> dict:
        result: dict = {}
        if self.managed_object is not None:
            result["managedObject"] = from_union([lambda x: to_class(ReferenceManagedObject, x), from_none],
                                                 self.managed_object)
        if self.reference_self is not None:
            result["self"] = from_union([from_str, from_none], self.reference_self)
        return result


@dataclass
class AdditionParents:
    references: Optional[List[Reference]] = None
    addition_parents_self: Optional[str] = None

    @staticmethod
    def from_dict(obj: Any) -> 'AdditionParents':
        # assert isinstance(obj, dict)
        references = from_union([lambda x: from_list(Reference.from_dict, x), from_none], obj.get("references"))
        addition_parents_self = from_union([from_str, from_none], obj.get("self"))
        return AdditionParents(references, addition_parents_self)

    def to_dict(self) -> dict:
        result: dict = {}
        if self.references is not None:
            result["references"] = from_union([lambda x: from_list(lambda x: to_class(Reference, x), x), from_none],
                                              self.references)
        if self.addition_parents_self is not None:
            result["self"] = from_union([from_str, from_none], self.addition_parents_self)
        return result


@dataclass
class Group:
    pass

    @staticmethod
    def from_dict(obj: Any) -> 'Group':
        # assert isinstance(obj, dict)
        return Group()

    def to_dict(self) -> dict:
        result: dict = {}
        return result


@dataclass
class FillLevelReport:
    create_report: Optional[bool] = None
    recipients: Optional[str] = None

    @staticmethod
    def from_dict(obj: Any) -> 'FillLevelReport':
        # assert isinstance(obj, dict)
        create_report = from_union([from_bool, from_none], obj.get("createReport"))
        recipients = from_union([from_str, from_none], obj.get("recipients"))
        return FillLevelReport(create_report, recipients)

    def to_dict(self) -> dict:
        result: dict = {}
        if self.create_report is not None:
            result["createReport"] = from_union([from_bool, from_none], self.create_report)
        if self.recipients is not None:
            result["recipients"] = from_union([from_str, from_none], self.recipients)
        return result


@dataclass
class ManagedObjectElement:
    id: Optional[int] = None
    addition_parents: Optional[AdditionParents] = None
    owner: Optional[str] = None
    child_devices: Optional[AdditionParents] = None
    child_assets: Optional[AdditionParents] = None
    creation_time: Optional[datetime] = None
    type: Optional[str] = None
    last_updated: Optional[datetime] = None
    child_additions: Optional[AdditionParents] = None
    name: Optional[str] = None
    device_parents: Optional[AdditionParents] = None
    asset_parents: Optional[AdditionParents] = None
    managed_object_self: Optional[str] = None
    c8_y_is_device_group: Optional[Group] = None
    toil_is_container_group: Optional[Group] = None
    fill_level_report: Optional[FillLevelReport] = None

    @staticmethod
    def from_dict(obj: Any) -> 'ManagedObjectElement':
        # assert isinstance(obj, dict)
        id = from_union([from_none, lambda x: int(from_str(x))], obj.get("id"))
        addition_parents = from_union([AdditionParents.from_dict, from_none], obj.get("additionParents"))
        owner = from_union([from_str, from_none], obj.get("owner"))
        child_devices = from_union([AdditionParents.from_dict, from_none], obj.get("childDevices"))
        child_assets = from_union([AdditionParents.from_dict, from_none], obj.get("childAssets"))
        creation_time = from_union([from_datetime, from_none], obj.get("creationTime"))
        type = from_union([from_str, from_none], obj.get("type"))
        last_updated = from_union([from_datetime, from_none], obj.get("lastUpdated"))
        child_additions = from_union([AdditionParents.from_dict, from_none], obj.get("childAdditions"))
        name = from_union([from_str, from_none], obj.get("name"))
        device_parents = from_union([AdditionParents.from_dict, from_none], obj.get("deviceParents"))
        asset_parents = from_union([AdditionParents.from_dict, from_none], obj.get("assetParents"))
        managed_object_self = from_union([from_str, from_none], obj.get("self"))
        c8_y_is_device_group = from_union([Group.from_dict, from_none], obj.get("c8y_IsDeviceGroup"))
        toil_is_container_group = from_union([Group.from_dict, from_none], obj.get("toil_IsContainerGroup"))
        fill_level_report = from_union([FillLevelReport.from_dict, from_none], obj.get("fillLevelReport"))
        return ManagedObjectElement(id, addition_parents, owner, child_devices, child_assets, creation_time, type,
                                    last_updated, child_additions, name, device_parents, asset_parents,
                                    managed_object_self, c8_y_is_device_group, toil_is_container_group,
                                    fill_level_report)

    def to_dict(self) -> dict:
        result: dict = {}
        if self.id is not None:
            result["id"] = from_union([lambda x: from_none((lambda x: is_type(type(None), x))(x)),
                                       lambda x: from_str((lambda x: str((lambda x: is_type(int, x))(x)))(x))], self.id)
        if self.addition_parents is not None:
            result["additionParents"] = from_union([lambda x: to_class(AdditionParents, x), from_none],
                                                   self.addition_parents)
        if self.owner is not None:
            result["owner"] = from_union([from_str, from_none], self.owner)
        if self.child_devices is not None:
            result["childDevices"] = from_union([lambda x: to_class(AdditionParents, x), from_none], self.child_devices)
        if self.child_assets is not None:
            result["childAssets"] = from_union([lambda x: to_class(AdditionParents, x), from_none], self.child_assets)
        if self.creation_time is not None:
            result["creationTime"] = from_union([lambda x: x.isoformat(), from_none], self.creation_time)
        if self.type is not None:
            result["type"] = from_union([from_str, from_none], self.type)
        if self.last_updated is not None:
            result["lastUpdated"] = from_union([lambda x: x.isoformat(), from_none], self.last_updated)
        if self.child_additions is not None:
            result["childAdditions"] = from_union([lambda x: to_class(AdditionParents, x), from_none],
                                                  self.child_additions)
        if self.name is not None:
            result["name"] = from_union([from_str, from_none], self.name)
        if self.device_parents is not None:
            result["deviceParents"] = from_union([lambda x: to_class(AdditionParents, x), from_none],
                                                 self.device_parents)
        if self.asset_parents is not None:
            result["assetParents"] = from_union([lambda x: to_class(AdditionParents, x), from_none], self.asset_parents)
        if self.managed_object_self is not None:
            result["self"] = from_union([from_str, from_none], self.managed_object_self)
        if self.c8_y_is_device_group is not None:
            result["c8y_IsDeviceGroup"] = from_union([lambda x: to_class(Group, x), from_none],
                                                     self.c8_y_is_device_group)
        if self.toil_is_container_group is not None:
            result["toil_IsContainerGroup"] = from_union([lambda x: to_class(Group, x), from_none],
                                                         self.toil_is_container_group)
        if self.fill_level_report is not None:
            result["fillLevelReport"] = from_union([lambda x: to_class(FillLevelReport, x), from_none],
                                                   self.fill_level_report)
        return result


@dataclass
class Statistics:
    page_size: Optional[int] = None
    current_page: Optional[int] = None

    @staticmethod
    def from_dict(obj: Any) -> 'Statistics':
        # assert isinstance(obj, dict)
        page_size = from_union([from_int, from_none], obj.get("pageSize"))
        current_page = from_union([from_int, from_none], obj.get("currentPage"))
        return Statistics(page_size, current_page)

    def to_dict(self) -> dict:
        result: dict = {}
        if self.page_size is not None:
            result["pageSize"] = from_union([from_int, from_none], self.page_size)
        if self.current_page is not None:
            result["currentPage"] = from_union([from_int, from_none], self.current_page)
        return result


@dataclass
class GroupPage:
    next: Optional[str] = None
    page_self: Optional[str] = None
    managed_objects: Optional[List[ManagedObjectElement]] = None
    statistics: Optional[Statistics] = None

    @staticmethod
    def from_dict(obj: Any) -> 'GroupPage':
        # assert isinstance(obj, dict)
        next = from_union([from_str, from_none], obj.get("next"))
        page_self = from_union([from_str, from_none], obj.get("self"))
        managed_objects = from_union([lambda x: from_list(ManagedObjectElement.from_dict, x), from_none],
                                     obj.get("managedObjects"))
        statistics = from_union([Statistics.from_dict, from_none], obj.get("statistics"))
        return GroupPage(next, page_self, managed_objects, statistics)

    def to_dict(self) -> dict:
        result: dict = {}
        if self.next is not None:
            result["next"] = from_union([from_str, from_none], self.next)
        if self.temperatures_self is not None:
            result["self"] = from_union([from_str, from_none], self.temperatures_self)
        if self.managed_objects is not None:
            result["managedObjects"] = from_union(
                [lambda x: from_list(lambda x: to_class(ManagedObjectElement, x), x), from_none], self.managed_objects)
        if self.statistics is not None:
            result["statistics"] = from_union([lambda x: to_class(Statistics, x), from_none], self.statistics)
        return result


def group_page_from_dict(s: Any) -> GroupPage:
    return GroupPage.from_dict(s)


def group_page_to_dict(x: GroupPage) -> Any:
    return to_class(GroupPage, x)
