import dateutil.parser
from dataclasses import dataclass
from datetime import datetime
from typing import Optional, Any, List, TypeVar, Type, cast, Callable

T = TypeVar("T")


def from_none(x: Any) -> Any:
    # assert x is None
    return x


def from_str(x: Any) -> str:
    # assert isinstance(x, str)
    return x


def from_union(fs, x):
    for f in fs:
        try:
            return f(x)
        except:
            pass
    # assert False


def is_type(t: Type[T], x: Any) -> T:
    # assert isinstance(x, t)
    return x


def from_int(x: Any) -> int:
    # assert isinstance(x, int) and not isinstance(x, bool)
    return x


def to_class(c: Type[T], x: Any) -> dict:
    # assert isinstance(x, c)
    return cast(Any, x).to_dict()


def from_datetime(x: Any) -> datetime:
    return dateutil.parser.parse(x)


def from_list(f: Callable[[Any], T], x: Any) -> List[T]:
    # assert isinstance(x, list)
    return [f(y) for y in x]


@dataclass
class Source:
    id: Optional[int] = None
    source_self: Optional[str] = None

    @staticmethod
    def from_dict(obj: Any) -> 'Source':
        # assert isinstance(obj, dict)
        id = from_union([from_none, lambda x: int(from_str(x))], obj.get("id"))
        source_self = from_union([from_str, from_none], obj.get("self"))
        return Source(id, source_self)

    def to_dict(self) -> dict:
        result: dict = {}
        if self.id is not None:
            result["id"] = from_union([lambda x: from_none((lambda x: is_type(type(None), x))(x)),
                                       lambda x: from_str((lambda x: str((lambda x: is_type(int, x))(x)))(x))], self.id)
        if self.source_self is not None:
            result["self"] = from_union([from_str, from_none], self.source_self)
        return result


@dataclass
class AlternativeDistance:
    unit: Optional[str] = None
    value: Optional[int] = None

    @staticmethod
    def from_dict(obj: Any) -> 'AlternativeDistance':
        # assert isinstance(obj, dict)
        unit = from_union([from_str, from_none], obj.get("unit"))
        value = from_union([from_int, from_none], obj.get("value"))
        return AlternativeDistance(unit, value)

    def to_dict(self) -> dict:
        result: dict = {}
        if self.unit is not None:
            result["unit"] = from_union([from_str, from_none], self.unit)
        if self.value is not None:
            result["value"] = from_union([from_int, from_none], self.value)
        return result


@dataclass
class ToilLevelMeterMeasurement:
    max_temperature: Optional[AlternativeDistance] = None
    current_temperature: Optional[AlternativeDistance] = None
    min_distance: Optional[AlternativeDistance] = None
    min_temperature: Optional[AlternativeDistance] = None
    max_distance: Optional[AlternativeDistance] = None
    alternative_distance: Optional[AlternativeDistance] = None
    current_distance: Optional[AlternativeDistance] = None
    axis_acceleration_mg_x: Optional[AlternativeDistance] = None
    axis_acceleration_mg_y: Optional[AlternativeDistance] = None
    axis_acceleration_mg_z: Optional[AlternativeDistance] = None

    @staticmethod
    def from_dict(obj: Any) -> 'ToilLevelMeterMeasurement':
        # assert isinstance(obj, dict)
        max_temperature = from_union([AlternativeDistance.from_dict, from_none], obj.get("maxTemperature"))
        current_temperature = from_union([AlternativeDistance.from_dict, from_none], obj.get("currentTemperature"))
        min_distance = from_union([AlternativeDistance.from_dict, from_none], obj.get("minDistance"))
        min_temperature = from_union([AlternativeDistance.from_dict, from_none], obj.get("minTemperature"))
        max_distance = from_union([AlternativeDistance.from_dict, from_none], obj.get("maxDistance"))
        alternative_distance = from_union([AlternativeDistance.from_dict, from_none], obj.get("alternativeDistance"))
        current_distance = from_union([AlternativeDistance.from_dict, from_none], obj.get("currentDistance"))
        axis_acceleration_mg_x = from_union([AlternativeDistance.from_dict, from_none], obj.get("axisAccelerationMgX"))
        axis_acceleration_mg_y = from_union([AlternativeDistance.from_dict, from_none], obj.get("axisAccelerationMgY"))
        axis_acceleration_mg_z = from_union([AlternativeDistance.from_dict, from_none], obj.get("axisAccelerationMgZ"))
        return ToilLevelMeterMeasurement(max_temperature, current_temperature, min_distance, min_temperature,
                                         max_distance, alternative_distance, current_distance, axis_acceleration_mg_x,
                                         axis_acceleration_mg_y, axis_acceleration_mg_z)

    def to_dict(self) -> dict:
        result: dict = {}
        if self.max_temperature is not None:
            result["maxTemperature"] = from_union([lambda x: to_class(AlternativeDistance, x), from_none],
                                                  self.max_temperature)
        if self.current_temperature is not None:
            result["currentTemperature"] = from_union([lambda x: to_class(AlternativeDistance, x), from_none],
                                                      self.current_temperature)
        if self.min_distance is not None:
            result["minDistance"] = from_union([lambda x: to_class(AlternativeDistance, x), from_none],
                                               self.min_distance)
        if self.min_temperature is not None:
            result["minTemperature"] = from_union([lambda x: to_class(AlternativeDistance, x), from_none],
                                                  self.min_temperature)
        if self.max_distance is not None:
            result["maxDistance"] = from_union([lambda x: to_class(AlternativeDistance, x), from_none],
                                               self.max_distance)
        if self.alternative_distance is not None:
            result["alternativeDistance"] = from_union([lambda x: to_class(AlternativeDistance, x), from_none],
                                                       self.alternative_distance)
        if self.current_distance is not None:
            result["currentDistance"] = from_union([lambda x: to_class(AlternativeDistance, x), from_none],
                                                   self.current_distance)
        if self.axis_acceleration_mg_x is not None:
            result["axisAccelerationMgX"] = from_union([lambda x: to_class(AlternativeDistance, x), from_none],
                                                       self.axis_acceleration_mg_x)
        if self.axis_acceleration_mg_y is not None:
            result["axisAccelerationMgY"] = from_union([lambda x: to_class(AlternativeDistance, x), from_none],
                                                       self.axis_acceleration_mg_y)
        if self.axis_acceleration_mg_z is not None:
            result["axisAccelerationMgZ"] = from_union([lambda x: to_class(AlternativeDistance, x), from_none],
                                                       self.axis_acceleration_mg_z)
        return result


@dataclass
class ToilOperationalMeasurement:
    communication_sequence_tries: Optional[AlternativeDistance] = None
    watch_dog_resets: Optional[AlternativeDistance] = None

    @staticmethod
    def from_dict(obj: Any) -> 'ToilOperationalMeasurement':
        # assert isinstance(obj, dict)
        communication_sequence_tries = from_union([AlternativeDistance.from_dict, from_none],
                                                  obj.get("communicationSequenceTries"))
        watch_dog_resets = from_union([AlternativeDistance.from_dict, from_none], obj.get("watchDogResets"))
        return ToilOperationalMeasurement(communication_sequence_tries, watch_dog_resets)

    def to_dict(self) -> dict:
        result: dict = {}
        if self.communication_sequence_tries is not None:
            result["communicationSequenceTries"] = from_union([lambda x: to_class(AlternativeDistance, x), from_none],
                                                              self.communication_sequence_tries)
        if self.watch_dog_resets is not None:
            result["watchDogResets"] = from_union([lambda x: to_class(AlternativeDistance, x), from_none],
                                                  self.watch_dog_resets)
        return result


@dataclass
class Measurement:
    id: Optional[int] = None
    measurement_self: Optional[str] = None
    time: Optional[datetime] = None
    source: Optional[Source] = None
    type: Optional[str] = None
    toil_operational_measurement: Optional[ToilOperationalMeasurement] = None
    toil_level_meter_measurement: Optional[ToilLevelMeterMeasurement] = None

    @staticmethod
    def from_dict(obj: Any) -> 'Measurement':
        # assert isinstance(obj, dict)
        id = from_union([from_none, lambda x: int(from_str(x))], obj.get("id"))
        measurement_self = from_union([from_str, from_none], obj.get("self"))
        time = from_union([from_datetime, from_none], obj.get("time"))
        source = from_union([Source.from_dict, from_none], obj.get("source"))
        type = from_union([from_str, from_none], obj.get("type"))
        toil_operational_measurement = from_union([ToilOperationalMeasurement.from_dict, from_none],
                                                  obj.get("toil_OperationalMeasurement"))
        toil_level_meter_measurement = from_union([ToilLevelMeterMeasurement.from_dict, from_none],
                                                  obj.get("toil_LevelMeterMeasurement"))
        return Measurement(id, measurement_self, time, source, type, toil_operational_measurement,
                           toil_level_meter_measurement)

    def to_dict(self) -> dict:
        result: dict = {}
        if self.id is not None:
            result["id"] = from_union([lambda x: from_none((lambda x: is_type(type(None), x))(x)),
                                       lambda x: from_str((lambda x: str((lambda x: is_type(int, x))(x)))(x))], self.id)
        if self.measurement_self is not None:
            result["self"] = from_union([from_str, from_none], self.measurement_self)
        if self.time is not None:
            result["time"] = from_union([lambda x: x.isoformat(), from_none], self.time)
        if self.source is not None:
            result["source"] = from_union([lambda x: to_class(Source, x), from_none], self.source)
        if self.type is not None:
            result["type"] = from_union([from_str, from_none], self.type)
        if self.toil_operational_measurement is not None:
            result["toil_OperationalMeasurement"] = from_union(
                [lambda x: to_class(ToilOperationalMeasurement, x), from_none], self.toil_operational_measurement)
        if self.toil_level_meter_measurement is not None:
            result["toil_LevelMeterMeasurement"] = from_union(
                [lambda x: to_class(ToilLevelMeterMeasurement, x), from_none], self.toil_level_meter_measurement)
        return result


@dataclass
class Statistics:
    page_size: Optional[int] = None
    current_page: Optional[int] = None

    @staticmethod
    def from_dict(obj: Any) -> 'Statistics':
        # assert isinstance(obj, dict)
        page_size = from_union([from_int, from_none], obj.get("pageSize"))
        current_page = from_union([from_int, from_none], obj.get("currentPage"))
        return Statistics(page_size, current_page)

    def to_dict(self) -> dict:
        result: dict = {}
        if self.page_size is not None:
            result["pageSize"] = from_union([from_int, from_none], self.page_size)
        if self.current_page is not None:
            result["currentPage"] = from_union([from_int, from_none], self.current_page)
        return result


@dataclass
class MeasurementPage:
    next: Optional[str] = None
    temperatures_self: Optional[str] = None
    statistics: Optional[Statistics] = None
    measurements: Optional[List[Measurement]] = None

    @staticmethod
    def from_dict(obj: Any) -> 'MeasurementPage':
        # assert isinstance(obj, dict)
        next = from_union([from_str, from_none], obj.get("next"))
        measurement_page_self = from_union([from_str, from_none], obj.get("self"))
        statistics = from_union([Statistics.from_dict, from_none], obj.get("statistics"))
        measurements = from_union([lambda x: from_list(Measurement.from_dict, x), from_none], obj.get("measurements"))
        return MeasurementPage(next, measurement_page_self, statistics, measurements)

    def to_dict(self) -> dict:
        result: dict = {}
        if self.next is not None:
            result["next"] = from_union([from_str, from_none], self.next)
        if self.temperatures_self is not None:
            result["self"] = from_union([from_str, from_none], self.temperatures_self)
        if self.statistics is not None:
            result["statistics"] = from_union([lambda x: to_class(Statistics, x), from_none], self.statistics)
        if self.measurements is not None:
            result["measurements"] = from_union([lambda x: from_list(lambda x: to_class(Measurement, x), x), from_none],
                                                self.measurements)
        return result


def measurement_page_from_dict(s: Any) -> MeasurementPage:
    return MeasurementPage.from_dict(s)


def measurement_page_to_dict(x: MeasurementPage) -> Any:
    return to_class(MeasurementPage, x)
