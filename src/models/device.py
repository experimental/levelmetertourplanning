from dataclasses import dataclass
import datetime


@dataclass
class Device:
    name = str = None
    id = str = None
    lat = int = 0
    lon = int = 0
    hysterese = int = 0
    warning = int = 0
    critical = int = 0
    empty = int = 0
    child_name = str = None
    child_id = str = None
    time = datetime = None

    def __init__(self):
        pass

    def __str__(self):
        device_str = f'name: {self.name}, id: {self.id}, lat: {self.lat}, lon: {self.lon}, hysterese: {self.hysterese}, warning: {self.warning}, empty: {self.empty}, child_name: {self.child_name}, child_id: {self.child_id}'
        return device_str

    def __repr__(self):
        device_str = f'name: {self.name}, id: {self.id}, lat: {self.lat}, lon: {self.lon}, hysterese: {self.hysterese}, warning: {self.warning}, empty: {self.empty}, child_name: {self.child_name}, child_id: {self.child_id}'
        return device_str
