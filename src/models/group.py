from dataclasses import dataclass


@dataclass
class Group:
    name = str = None
    id = str = None
    child_name = str = None
    child_id = str = None

    def __init__(self):
        pass

    def __str__(self):
        group_str = f'name: {self.name}, id: {self.id}, child_name: {self.child_name}, child_id: {self.child_id}'
        return group_str

    def __repr__(self):
        group_str = f'name: {self.name}, id: {self.id}, child_name: {self.child_name}, child_id: {self.child_id}'
        return group_str
