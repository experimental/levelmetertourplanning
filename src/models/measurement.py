from dataclasses import dataclass


@dataclass
class Measurement:
    unit = str = None
    id = str = None
    time = datetime = None
    value = float = None
    source_id = str = None

    def __init__(self):
        pass

    def __str__(self):
        measurement_str = f'id: {self.id}, unit: {self.unit}, time: {self.time}, value: {self.value}, source_id: {self.source_id}'
        return measurement_str

    def __repr__(self):
        measurement_str = f'id: {self.id}, unit: {self.unit}, time: {self.time}, value: {self.value}, source_id: {self.source_id}'
        return measurement_str
