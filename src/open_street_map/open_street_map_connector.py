# -*- coding: utf-8 -*-
"""
Copyright Open Logistics Foundation

Licensed under the Open Logistics Foundation License 1.3
For details on the licensing terms, see the LICENSE file.
SPDX-License-Identifier: OLFL-1.3

@author: holger leichsenring
"""
import requests
from core.environment_variable_handler import EnvironmentVariableHandler
from core.url_builder import UrlBuilder


class OpenStreetMapConnector():
    '''
    Connect to the OpenStreetMap
    '''

    def __init__(self):
        self.url_builder = UrlBuilder()
        self.environment_variable_handler = EnvironmentVariableHandler()
        self.environment_variable_handler.validate()

        self.headers = {
            'Accept': 'application/json, application/geo+json, application/gpx+xml, img/png; charset=utf-8'
        }

    def get_distance_matrix(self, coordinates):
        url = self.url_builder.create_get_distance_matrix()
        json = {'locations': coordinates}

        response = requests.post(url=url,
                                 json=json,
                                 headers=self.headers)
        if not response.ok:
            response.raise_for_status()

        return response.text

    def get_geo(self, coordinates):
        url = self.url_builder.create_get_distance_geo()
        body = {'coordinates': coordinates, 'units': 'km', 'language': 'de'}

        response = requests.post(url=url,
                                 json=body,
                                 headers=self.headers)
        if not response.ok:
            response.raise_for_status()

        return response.text, response.json()
